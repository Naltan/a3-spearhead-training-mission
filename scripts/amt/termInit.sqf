params ["_terminal", "_target"];
{
	_name = _x select 0;
	_params = _x select 1;
	_target = _x select 2;

	_terminal addAction [
		_name,
		{
			_itr = (_this select 3) select 1;
			_arr = (_this select 3) select 2;
			_target = (_this select 3) select 3;
			[_target, _arr, _itr, _this select 0] call compile preprocessFileLineNumbers "scripts\amt\patientSpawner.sqf";
		},
		_x,
		100,
		true,
		true,
		"",
		"true",
		5
	];

} forEach [
//This is where you can edit the difficulty settings
	["<t color='#99ff99'>Spawn Simple Patient</t>",		1, [0,0.5,0.7,0.9], _target],
	["<t color='#00ff00'>Spawn Easy Patient</t>",		2, [0,0.5,0.7,0.9], _target],
	["<t color='#ffff00'>Spawn Medium Patient</t>",		3, [0,0.5,0.7,0.9], _target],
	["<t color='#ff9900'>Spawn Difficult Patient</t>",	5, [0,0.5,0.85,0.9], _target],
	["<t color='#ff0000'>Spawn Extreme Patient</t>",	8, [0,0.5,0.85,0.9], _target],
	["<t color='#990000'>Spawn Hardcore Patient</t>",	10, [0.5,0.7,0.9,0.99], _target]
];
