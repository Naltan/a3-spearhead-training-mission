params ["_flagpole"];

_genInteraction = {
	params ["_flagpole", "_text", "_destpole"];
	_flagpole addAction [
		["<t color='#33ff33'>Teleport to", _text, "</t>"] joinString " ",
		{
			_destpole = _this select 3 select 0;
			_player = _this select 1;
			
			_player setpos getpos _destpole;
		},
		[_destpole],
		100,
		true,
		true,
		"",
		"true",
		10
	];
};

_destinationArray = [
	["Spawn", tp_spawn],
	["Vehicle Pool", tp_vicpool],
	["Medical Training", tp_medical],
	["Range - AT", tp_at],
	["Range - Diving", tp_diving],
	["Range - EOD", tp_eod],
	["Range - Grenades", tp_grenade],
	["Range - Marksman", tp_marksman],
	["Range - Mounted Guns", tp_mountedgun],
	["Range - Rifle/MG", tp_riflerange],
	["Live Fire - Convoy Ambush", tp_lf_ambush_convoy],
	["Live Fire - MOUT", tp_lf_mout],
	["Live Fire - Urban Ambush", tp_lf_ambush_urban],
	["Air - Runway", tp_airbase],
	["Air - Heliport", tp_heliport],
	["Artillery - Position 1", tp_arty1],
	["Artillery - Position 2", tp_arty2],
	["Land Navigation", tp_land_nav]
];

{
	if (_x select 1 != _flagpole) then {
		[_flagpole, _x select 0, _x select 1] call _genInteraction;
	};
} forEach (_destinationArray);
